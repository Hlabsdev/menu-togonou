import { Component, OnInit } from '@angular/core';

import { Menu } from './../menu';

@Component({
  selector: 'app-menu-detail',
  templateUrl: './menu-detail.component.html',
  styleUrls: ['./menu-detail.component.css']
})
export class MenuDetailComponent implements OnInit {
  public menu = new Menu("Akoume", "https://guillaumeinternoscia.files.wordpress.com/2013/07/p1030402.jpg", "Ici il n’est pas question de cervelles de singes ou de brochettes de chien, d’ailleurs la viande de chiens je trouve que ça sent mauvais et selon mes goûts c’est encore moins intéressant que la tête de mouton. Le rat par contre c’est pas si mal.");

  constructor() { }

  ngOnInit(): void {
  }

}
